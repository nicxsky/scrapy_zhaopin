"""TestDjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os
from django.contrib import admin
from django.conf.urls import url
from zp import views as zp_views
from DataView import settings
if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    from django.conf.urls.static import static
urlpatterns = [
    url('admin/', admin.site.urls, name='admin'),
    url(r'^$', zp_views.home, name='home'),
    url('zwyx/dd_index/', zp_views.zwyx_dd, name='zwyx_dd'),
    url('zwyx/xl_index/', zp_views.zwyx_xl, name='zwyx_xl'),
    url('zwyx/gsgm_index/', zp_views.zwyx_gsgm, name='zwyx_gsgm'),
    url('zwyx/gsxz_index/', zp_views.zwyx_gsxz, name='zwyx_gsxz'),
    url('zwyx/gshy_index/', zp_views.zwyx_gshy, name='zwyx_gshy'),
    url('no_found/', zp_views.no_found, name='no_found'),
    url('zwyx/type_index/', zp_views.zwyx_type, name='type_index'),
    url('zwyx/zwyx_zw_count/', zp_views.zwyx_zw_count, name='zwyx_zw_count'),
    url('zp/wordcloud/', zp_views.zp_word, name='zp_word')
]
if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
if not settings.DEBUG:
    handler404 = "zp.views.page_not_found"
    handler500 = "zp.views.sever_error"